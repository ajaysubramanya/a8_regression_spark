package neu.mr.cs6240.a8_regression_spark;

import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import neu.mr.cs6240.a8_regression_spark.functions.CarrierFlightMap;
import neu.mr.cs6240.a8_regression_spark.functions.CheapCarrierYearMap;
import neu.mr.cs6240.a8_regression_spark.functions.CheapestCarrier;
import neu.mr.cs6240.a8_regression_spark.functions.CheapestCarrierFliter;
import neu.mr.cs6240.a8_regression_spark.functions.CheapestCarrierMap;
import neu.mr.cs6240.a8_regression_spark.functions.SanityFilter;
import neu.mr.cs6240.a8_regression_spark.functions.WeekCarrierMap;
import neu.mr.cs6240.a8_regression_spark.functions.WeekPriceSortedMap;
import neu.mr.cs6240.a8_regression_spark.utils.CLI;
import neu.mr.cs6240.a8_regression_spark.utils.Flight;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.*;
import scala.Tuple2;

/**
 * This is the main class that configures and runs the Spark program.
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 */
public class Regression {

	public static void main(String[] args) {		
		CLI cli = new CLI(args);
		runSpark(cli);
	}
	
	/**
	 * This method configures and runs the Spark program.
	 * @param cli
	 */
	private static void runSpark(CLI cli) {
		SparkConf conf = new SparkConf().setAppName("Regression");
		JavaSparkContext sc = new JavaSparkContext(conf);

		//Read the files
		JavaRDD<String> distFile = sc.textFile(cli.getInputDir());

		// cache the RDD to be used again for finding the median price
		distFile.cache();
		
		//Filter the rows by sanity check 
		JavaRDD<String> saneLines = distFile.filter(new SanityFilter());
		//Create RDD Pair<Year, FlightInfo>
		JavaPairRDD<String, Flight> mapperOp = saneLines.mapToPair(new CarrierFlightMap());
		//Create RDD <Year, Cheapest Carrier/Flight>
		JavaPairRDD<String, String> cheapFlightPerYr = mapperOp.groupByKey()
				.mapValues(new CheapCarrierYearMap(cli.getTime()));

		//Get Cheapest Carrier of a Year and the Year List from the RDD
		List<String> cheapestAllLines = cheapFlightPerYr.flatMap(new CheapestCarrierMap()).collect();

		CheapestCarrier cc = new CheapestCarrier();
		List<String> cheapestCarrierList = cc.findCheapestCarrier(cheapestAllLines);
		//Output the Cheapest Carrier and the time argument passed by the user
		sc.parallelize(cheapestCarrierList).saveAsTextFile(cli.getOutputDir() + INTERMEDIATE_OP);
		
		//Filter the sane rows to get the cheapest carrier rows
		JavaRDD<String> carrierEntries = saneLines.filter(new CheapestCarrierFliter(cheapestCarrierList.get(FIRST_ELEMENT)));

		//get the key value pair to find list of prices by year and week
		JavaPairRDD<Tuple2<Integer, Integer>, Double> weekPrice = carrierEntries.mapToPair(new WeekCarrierMap());
		
		//group by the year and week composite key and get the median price for each key 
		JavaPairRDD<Tuple2<Integer, Integer>, Double> sorted = weekPrice.groupByKey().flatMapValues(new WeekPriceSortedMap());

		//Output the Year, Week and Mean Price
		sorted.saveAsTextFile(cli.getOutputDir());
		sc.close();

	}

}
