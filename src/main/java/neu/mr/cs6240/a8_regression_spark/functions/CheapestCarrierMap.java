package neu.mr.cs6240.a8_regression_spark.functions;

import java.util.Arrays;

import org.apache.spark.api.java.function.FlatMapFunction;

import scala.Tuple2;

/**
 * Flattens the data from all RDDs which calculated cheapest carrier for the
 * year into a Iterable<String> where each String comprises "year,carrier".
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class CheapestCarrierMap implements FlatMapFunction<Tuple2<String, String>, String> {
	private static final long serialVersionUID = 1L;

	@Override
	public Iterable<String> call(Tuple2<String, String> t) throws Exception {
		return Arrays.asList((t._1 + "," + t._2).split("\n"));
	}
}