package neu.mr.cs6240.a8_regression_spark.functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.spark.api.java.function.Function;

import neu.mr.cs6240.a8_regression_spark.utils.CompareUtils;
import neu.mr.cs6240.a8_regression_spark.utils.CompareUtilsEnum;
import neu.mr.cs6240.a8_regression_spark.utils.DataPoint;
import neu.mr.cs6240.a8_regression_spark.utils.Flight;
import neu.mr.cs6240.a8_regression_spark.utils.LinearRegression;

/**
 * This is a class to get the cheapest airline for a year RDD. Uses linear
 * regression to calculate slope and intercept values. And finds average price
 * for a given N value for a carrier and choosing cheapest.
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class CheapCarrierYearMap implements Function<Iterable<Flight>, String> {
	private static final long serialVersionUID = 1L;
	private int scheduledTime;

	public CheapCarrierYearMap(int scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	@Override
	public String call(Iterable<Flight> rs) {
		Map<String, ArrayList<DataPoint>> hm = new HashMap<>();
		Map<String, Double> results = new HashMap<>();

		createMap(hm, rs);
		return getCheapestCarrier(hm, results);
	}

	/**
	 * This method creates a HashMap<Carrier, List<DataPoint>> taking Iterable
	 * <Flight>. Iterable<Flight> is list of all Flight for a given year.
	 *
	 * @param hm
	 *            Resulting HashMap with Carrier as key
	 * @param rs
	 *            Input List of Flight groupedBy year
	 */
	private void createMap(Map<String, ArrayList<DataPoint>> hm, Iterable<Flight> rs) {
		for (Flight f : rs) {
			String airlineName = f.getCarrierName().toString();
			if (hm.containsKey(airlineName)) {
				hm.get(airlineName).add(new DataPoint(f.getAvgPriceInCents(), f.getScheduledTime()));
			} else {
				ArrayList<DataPoint> dp = new ArrayList<>();
				dp.add(new DataPoint(f.getAvgPriceInCents(), f.getScheduledTime()));
				hm.put(f.getCarrierName().toString(), dp);
			}
		}
	}

	/**
	 * This method returns the cheapest carrier for the year. First linear
	 * regression(slope and intercept) on given data for a year(being the key)
	 * and airline is computed. Then given value of N to get the estimated price
	 * for a carrier in that year. Then cheapest avg price airline is picked as
	 * cheapest airline for the year.
	 *
	 * @param hm
	 *            <Carrier, List<DataPoint<avgP, sTime>>>
	 * @param results
	 *            <Carrier, Estimated AvgPrice>
	 * @return Cheapest airline among the results for the given year(i.e.key)
	 */
	private String getCheapestCarrier(Map<String, ArrayList<DataPoint>> hm, Map<String, Double> results) {
		int N = scheduledTime;

		Iterator<Entry<String, ArrayList<DataPoint>>> iterator = hm.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, ArrayList<DataPoint>> entry = iterator.next();
			results.put(entry.getKey(), LinearRegression.getRegressionValue(entry.getValue(), N));
		}

		Map<String, Double> sortedResults = CompareUtils.sortHashMapVals(results, CompareUtilsEnum.INCREASING);
		Entry<String, Double> MinPriceEntry = sortedResults.entrySet().iterator().next();

		return (MinPriceEntry.getKey().toString() + "," + MinPriceEntry.getValue().toString());
	}

}
