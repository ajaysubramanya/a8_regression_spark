package neu.mr.cs6240.a8_regression_spark.functions;

import static neu.mr.cs6240.a8_regression_spark.utils.Constants.HALF;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.ODD_COUNT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.spark.api.java.function.Function;

import com.google.common.collect.Lists;

/**
 * This class implements flatMapValues function which finds the median price from a
 * list of prices for a particular year week key.
 * 
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class WeekPriceSortedMap implements Function<Iterable<Double>, Iterable<Double>> {

	private static final long serialVersionUID = 1L;

	@Override
	public Iterable<Double> call(Iterable<Double> vals) throws Exception {
		List<Double> listOfPrice = Lists.newArrayList(vals);
		Collections.sort(listOfPrice);
		List<Double> retList = new ArrayList<Double>();
		int middle = listOfPrice.size() / HALF;
		
		if (listOfPrice.size() % HALF == ODD_COUNT)
			retList.add(listOfPrice.get(middle)); 
		else
			retList.add((listOfPrice.get(middle - ODD_COUNT) + listOfPrice.get(middle)) / HALF);
		
		return retList;
	}
}