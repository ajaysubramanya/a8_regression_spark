package neu.mr.cs6240.a8_regression_spark.functions;

import org.apache.spark.api.java.function.Function;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr.cs6240.a8_regression_spark.utils.AirRecordSanity;

/**
 * This class implements Filter to check if a given line is sane. Returns true
 * if line passes sanity test else false.
 *
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class SanityFilter implements Function<String, Boolean> {

	private static final long serialVersionUID = 1L;
	private AirRecordSanity sanity = new AirRecordSanity();

	@Override
	public Boolean call(String s) throws Exception {
		CSVParser parser = new CSVParser();
		return !sanity.sanityFails(parser.parseLine(s));
	}

}
