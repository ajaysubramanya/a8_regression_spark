package neu.mr.cs6240.a8_regression_spark.functions;

import static neu.mr.cs6240.a8_regression_spark.utils.Constants.CARRIER;

import org.apache.spark.api.java.function.Function;

import au.com.bytecode.opencsv.CSVParser;

/**
 * This class implements Filter to check if a given line contains cheapest
 * carrier. Returns true if line belongs to cheapest carrier else false.
 *
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class CheapestCarrierFliter implements Function<String, Boolean> {
	private static final long serialVersionUID = 1L;
	private String cheapestCarrier;

	public CheapestCarrierFliter(String cheapestCarrier) {
		this.cheapestCarrier = cheapestCarrier;
	}

	@Override
	public Boolean call(String s) throws Exception {
		String cc = cheapestCarrier;
		CSVParser parser = new CSVParser();
		return parser.parseLine(s)[CARRIER].equals(cc);
	}
}
