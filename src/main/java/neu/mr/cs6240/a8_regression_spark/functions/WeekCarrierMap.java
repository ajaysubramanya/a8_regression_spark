package neu.mr.cs6240.a8_regression_spark.functions;

import static neu.mr.cs6240.a8_regression_spark.utils.Constants.AVG_PRICE;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.DAY_OF_MONTH;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.MONTH;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.YEAR;
import neu.mr.cs6240.a8_regression_spark.utils.YearWeek;

import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;
import au.com.bytecode.opencsv.CSVParser;
/**
 * This class implements mapToPair function, which maps the sting record 
 * into a Key value pair, where key is a composite of Year and week and 
 * value is price for that given string record 
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class WeekCarrierMap implements PairFunction<String, Tuple2<Integer, Integer>, Double> {
	private static final long serialVersionUID = 1L;

	@Override
	public Tuple2<Tuple2<Integer, Integer>, Double> call(String rec) throws Exception {
		CSVParser parser = new CSVParser();
		String[] fields = parser.parseLine(rec);
		Integer year = new Integer(fields[YEAR]);
		Integer week = new YearWeek(fields[YEAR], fields[DAY_OF_MONTH], fields[MONTH]).getWeek();
		Double price = Double.parseDouble(fields[AVG_PRICE]);
		Tuple2<Integer, Integer> yw = new Tuple2<Integer, Integer>(year, week);
		return new Tuple2<Tuple2<Integer, Integer>, Double>(yw, price);
	}
}
