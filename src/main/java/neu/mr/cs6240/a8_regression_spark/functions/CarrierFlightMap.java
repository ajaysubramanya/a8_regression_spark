package neu.mr.cs6240.a8_regression_spark.functions;

import static neu.mr.cs6240.a8_regression_spark.utils.Constants.AVG_PRICE;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.CARRIER;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.CENTS;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.SCDL_TIME;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.YEAR;

import java.io.IOException;

import org.apache.spark.api.java.function.PairFunction;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr.cs6240.a8_regression_spark.utils.Flight;
import scala.Tuple2;

/**
 * This is a class to create RDD Pair<Year, Flight> by parsing each line in the
 * input file.
 * 
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class CarrierFlightMap implements PairFunction<String, String, Flight> {

	private static final long serialVersionUID = 1L;

	@Override
	public Tuple2<String, Flight> call(String line) throws IOException {
		CSVParser csvReader = new CSVParser(',', '"');
		String[] fields = csvReader.parseLine(line.toString());
		double avgInCents = Double.parseDouble(fields[AVG_PRICE]) * CENTS;
		return new Tuple2<String, Flight>(fields[YEAR],
				new Flight(fields[CARRIER], (int) avgInCents, Integer.parseInt(fields[SCDL_TIME])));

	}

}
