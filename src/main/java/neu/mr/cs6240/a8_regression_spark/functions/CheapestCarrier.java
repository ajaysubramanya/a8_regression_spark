package neu.mr.cs6240.a8_regression_spark.functions;

import static neu.mr.cs6240.a8_regression_spark.utils.Constants.CARRIER_NAME;
import static neu.mr.cs6240.a8_regression_spark.utils.Constants.INCREMENT_COUNTER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import au.com.bytecode.opencsv.CSVParser;
import neu.mr.cs6240.a8_regression_spark.utils.CompareUtils;
import neu.mr.cs6240.a8_regression_spark.utils.CompareUtilsEnum;

/**
 * This is a class finds the cheapest airline for all given years in the input
 * data.
 *
 * @author prasad memane
 * @author ajay subramanya
 * @author swapnil mahajan
 * @author smitha naresh
 **/
public class CheapestCarrier {

	private HashMap<String, Double> hm;
	private CSVParser csvReader;

	public CheapestCarrier() {
		this.hm = new HashMap<>();
		this.csvReader = new CSVParser(',', '"');
	}

	public List<String> findCheapestCarrier(List<String> cheapestAllLines) {
		for (String str : cheapestAllLines) {
			String[] eachLine;
			try {
				eachLine = csvReader.parseLine(str.toString());
				String carrierName = eachLine[CARRIER_NAME];
				if (hm.containsKey(carrierName)) {
					hm.put(carrierName, hm.get(carrierName) + INCREMENT_COUNTER);
				} else {
					hm.put(carrierName, INCREMENT_COUNTER);
				}

			} catch (IOException e) {
				new Error("IOException while parsing cheapest lines: " + e);
			}
		}

		Map<String, Double> sortedHm = CompareUtils.sortHashMapVals(hm, CompareUtilsEnum.DECREASING);
		Entry<String, Double> MaxValueEntry = sortedHm.entrySet().iterator().next();

		final String cheapestCarrier = MaxValueEntry.getKey();
		List<String> cheapestFlight = new ArrayList<>();
		cheapestFlight.add(cheapestCarrier);
		return cheapestFlight;
	}

}
