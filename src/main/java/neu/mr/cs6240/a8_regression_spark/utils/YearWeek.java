package neu.mr.cs6240.a8_regression_spark.utils;

import java.io.Serializable;
import java.util.Calendar;

/**
 * This is class for a custom key for the map function  
 * @author prasadmemane
 * @author swapnilmahajan
 */
public class YearWeek implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static int MONTH_OFFSET = 1;
	
	private int year;
	private int week;
	
	/**
	 * This is a constructor which takes in year, dayOfMonth and month and find the weekOfYear
	 * @param year
	 * @param dayOfMonth
	 * @param month
	 */
	public YearWeek(String year, String dayOfMonth, String month) {
		int yearInt = Integer.valueOf(year);
		int monthInt = Integer.valueOf(month) - MONTH_OFFSET;
		int dayOfMonthInt = Integer.valueOf(dayOfMonth);
		
		Calendar cal = Calendar.getInstance();
	    cal.set(yearInt, monthInt, dayOfMonthInt);
	    cal.setMinimalDaysInFirstWeek(1);
	    int weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);

		this.year = yearInt;
		this.week = weekOfYear;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	@Override
	public String toString() {
		return "YearWeek [year=" + year + ", week=" + week + "]";
	} 
	
	
}
