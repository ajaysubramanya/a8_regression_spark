package neu.mr.cs6240.a8_regression_spark.utils;

/**
 * This is a constants class
 * @author prasadmemane
 */
public class Constants {

	public static final int AVG_PRICE = 109;
	public static final int CARRIER = 6;
	public static final int YEAR = 0;
	public static final int MONTH = 2;
	public static final int DAY_OF_MONTH = 3;
	
	public static final int SCDL_TIME = 50;
	public static final int HALF = 2;
	public static final int ODD_COUNT = 1;
	public static final int CENTS = 100;
	
	public static final String INTERMEDIATE_OP = "_CHEAPEST_AIRLINE";
	public static final int CARRIER_NAME = 1;
	public static final Double INCREMENT_COUNTER = 1.0;
	public static final int FIRST_ELEMENT = 0;
	public static final int ARGS_LEN = 3;
	
	public static final int ERROR = -1;
		
}
