package neu.mr.cs6240.a8_regression_spark.utils;

import java.io.Serializable;

/**
 * This is a class which hold the flight info like carrier, avg_ticket_price and scheduled_time
 * @author prasadmemane
 *
 */
public class Flight implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Flight(String carrierName, int avgPriceInCents, int scheduledTime) {
		super();
		this.carrierName = carrierName;
		this.avgPriceInCents = avgPriceInCents;
		this.scheduledTime = scheduledTime;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public int getAvgPriceInCents() {
		return avgPriceInCents;
	}

	public void setAvgPriceInCents(int avgPriceInCents) {
		this.avgPriceInCents = avgPriceInCents;
	}

	public int getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(int scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	private String carrierName;
	private int avgPriceInCents;
	private int scheduledTime;

}
