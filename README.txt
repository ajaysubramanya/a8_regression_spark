################################################################################
# README-A8 REGRESSION SPARK
# Smitha Bangalore Naresh, Prasad Memane, Swapnil Mahajan, Ajay Subramanya

# bangalorenaresh{dot}s{at}husky{dot}neu{dot}edu
# memane{dot}p{at}husky{dot}neu{dot}edu
# mahajan{dot}sw{at}husky{dot}neu{dot}edu
# subramanya{dot}a{at}husky{dot}neu{dot}edu
################################################################################

################################  PREREQUISITES ################################

* Update the runCloud script with your s3 Paths. Instructions are present in the
  same file. We are using Professor bucket for input data(337 files)

* We are using Apache Maven 3.3.9 for dependency management and build automation

* Please `chmod +x runCloud` and `chmod +x runReport` if it is not already 
  executable

* NOTE: Make sure that aws CLI Default Output format is set to text
	<run script will not be able to pull the output without this setting>
  (To confirm it,
	 run "aws configure list" from the command prompt
	 from the output get the config-file and check in that
	 it shoud read as <output = text>
	
	if not, please run "aws configure" and set it to text)
    Example when you run the "aws configure" command.
        AWS Access Key ID [****************CU6A]: (Press Enter)
        AWS Secret Access Key [****************8Grk]: (Press Enter)
        Default region name [us-west-2]: (Press Enter)
        Default output format [json]: text (Change to text)
        
* Following packages are required to run the R docs that generates report.
  Please install those using the command specified

    dplyr --> install.packages("dplyr")
    ggplot --> install.packages("ggplot2")
    rmarkdown --> install.packages("rmarkdown")
    pandoc --> instructions at <http://pandoc.org/installing.html>

    also install the package texlite

################################## RUN SCRIPT ##################################

* `./runCloud` to compile the program locally and upload the jar to s3, and 
   run the program on the cluster, pull the data from S3 and plot graphs
   and generate the report.

* Look for folder's called `FinalOuptut` and `Carrier` for the results of the
  run. The graph for the run can be found in the file `AirMedPricebyWeek.pdf`

################################## CAVEATS #####################################

* Main class is Regression.java

* Project structure is as below, 
.
├── A8Report.pdf
├── AirMedPricebyWeek.Rmd
├── README.txt
├── SampleOutput
│   ├── AirMedPricebyWeek_N1.pdf
│   ├── AirMedPricebyWeek_N200.pdf
│   ├── sample_results_N1.txt
│   └── sample_results_N200.txt
├── pom.xml
├── runCloud
├── runReport
└── src
    ├── main
    └── test

4 directories, 10 files

* SampleOutput contains the Results and Graphs for Median price per week for 
  cheapest airline for N=1 and N=200.

################################### THE END ####################################
